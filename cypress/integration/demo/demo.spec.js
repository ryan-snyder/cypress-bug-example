// / <reference types='Cypress' />

describe('XHR Demo', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('Catches XHR request', () => {
        cy.server();
        // replace this url with the url you are testing with
        cy.route('https://webhook.site/e3e5989d-ca4d-456d-b6b3-5d31c6067955').as('request');
        cy.wait('@request');
    })
})